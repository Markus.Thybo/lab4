package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	IGrid currentGeneration;
	
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}




	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}

	}
	private int countNeighbors(int row, int col, CellState state) {
		int numNeighbours = 0;
		
		// nested forloop to go trough every cell next to a cell
		for(int x=-1; x<=1; x++) {
			for(int y=-1; y<=1; y++) {
				if(x==0 && y==0) {
					continue;
				}
				
				// checks that it is in the grid. 
				if(isInGrid(row+x,col+y)) {
					
					// finner ut hva cellstate til denne naboen er
					CellState neighbour = getCellState(row+x, col+y);
					
					if(neighbour==state) {
						numNeighbours++;
					}
				}
			}
		}
		
		return numNeighbours;
	}
	public boolean isInGrid(int row, int col) {
		
		if(row<0 || row>=numberOfRows())
			return false;
		if(col<0 || col>=numberOfColumns())
			return false;
		return true;
	}


	@Override
	public void step() {
		IGrid nextGrid = new CellGrid(currentGeneration.numRows(), currentGeneration.numColumns(),CellState.ALIVE);

		for(int i=0;i<numberOfRows();i++) {
			for(int j=0 ; j <numberOfColumns(); j++) {
				CellState state = this.getNextCell(i, j);
				nextGrid.set(i, j, state);
			}
		}

		currentGeneration = nextGrid;

	}

	@Override
	public CellState getNextCell(int row, int column) {
		// this is a set of rules that will check every cell. 
		
				// cehcks if the cell is dead or alive and crates a bollean.
		
		
		
				boolean Alive = getCellState(row, column) == CellState.ALIVE;		
				boolean Dead = getCellState(row, column) == CellState.DEAD;
				boolean Dying = getCellState(row, column) == CellState.DYING;		
				
				// count how many neigbhours that are alive.
				int livingNeighbours = countNeighbors(row,column,CellState.ALIVE);
				
				// herre i implenet the rules of game of life;
				if(Alive)
					
					return CellState.DYING;
				
				
				if(Dead && livingNeighbours == 2)		
					
					return CellState.ALIVE;
				
				if(Dead)
					return CellState.DEAD;
				
				if(Dying)
					return CellState.DEAD;
				
					
				return getCellState(row, column);
				
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;

	}




	@Override
	public CellState getCellState(int row, int column) {
		return currentGeneration.get(row, column);
	}

}

