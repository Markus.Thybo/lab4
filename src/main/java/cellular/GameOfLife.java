package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGrid = new CellGrid(currentGeneration.numRows(), currentGeneration.numColumns(),CellState.ALIVE);

		for(int i=0;i<numberOfRows();i++) {
			for(int j=0 ; j <numberOfColumns(); j++) {
				CellState state = this.getNextCell(i, j);
				nextGrid.set(i, j, state);
			}
		}

		currentGeneration = nextGrid;


	}

	@Override
	public CellState getNextCell(int row, int col) {
		// this is a set of rules that will check every cell. 
		
		// cehcks if the cell is dead or alive and crates a bollean. 
		boolean Alive = getCellState(row, col) == CellState.ALIVE;		
		boolean Dead = getCellState(row, col) == CellState.DEAD;
		
		// count how many neigbhours that are alive.
		int livingNeighbours = countNeighbors(row,col,CellState.ALIVE);
		
		// herre i implenet the rules of game of life;
		if(Alive && livingNeighbours < 2)
			
			return CellState.DEAD;
		
		if(Alive && livingNeighbours > 3)
			
			return CellState.DEAD;
		
		if(Dead && livingNeighbours == 3)		
			
			return CellState.ALIVE;
		
		if(Alive && (livingNeighbours == 2 || livingNeighbours==3))
			
			return CellState.ALIVE;
			
		return getCellState(row, col);
		
	
	}
	
	// this is a mothode just to check that the neigbohur is not outside the grid. 
	
	public boolean isInGrid(int row, int col) {
		
		if(row<0 || row>=numberOfRows())
			return false;
		if(col<0 || col>=numberOfColumns())
			return false;
		return true;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int numNeighbours = 0;
		
		// nested forloop to go trough every cell next to a cell
		for(int x=-1; x<=1; x++) {
			for(int y=-1; y<=1; y++) {
				if(x==0 && y==0) {
					continue;
				}
				
				// checks that it is in the grid. 
				if(isInGrid(row+x,col+y)) {
					
					// finner ut hva cellstate til denne naboen er
					CellState neighbour = getCellState(row+x, col+y);
					
					if(neighbour==state) {
						numNeighbours++;
					}
				}
			}
		}
		
		return numNeighbours;
	}


	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
