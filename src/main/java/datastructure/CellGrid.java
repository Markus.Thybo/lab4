package datastructure;


import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {
	

	public int rows;
	public int columns;
	ArrayList<CellState> grid;
	
	
	// Creates the grid. 
    public CellGrid(int rows, int columns, CellState initialState) {
    	
		this.rows = rows;
		this.columns = columns;
		this.grid = new ArrayList<CellState>();
		int totalCells = rows*columns;
		
		for (int i=0; i<totalCells; i++) {
			grid.add(initialState);
		}
 

	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    
    //sets the State of the Cell, but first i check that the cell is inside of the grid. 
    @Override
    public void set(int row, int column, CellState element) {
    	if(row >= this.numRows() || column>=this.numColumns()) {
			throw new IndexOutOfBoundsException("The posistion is out out bounds, to high ");
		}
		if(row < 0 || column<0) {
			throw new IndexOutOfBoundsException("The posistion is out out bounds, to low ");
		}
    	int position = (row*this.columns) + column;
    	grid.set(position, element);
    	

        
        
    }

    @Override
    public CellState get(int row, int column) {
    	
    	// first i make sure that the cell is not outside of the grid. 
    	if(row >= this.numRows() || column  >= this.numColumns()) {
			throw new IndexOutOfBoundsException("The posistion is out out bounds, to high ");
		}
		if(row < 0 || column < 0) {
			throw new IndexOutOfBoundsException("The posistion is out out bounds, to low ");
		}
		// gets the posistion of the cell. 
		int position =(row*this.columns) + column;
		return this.grid.get(position);

    }

    @Override
    public IGrid copy() {
    	
    	// makes a new cellgrid. 
		CellGrid copyGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
		
		// goes trough the entire grid and makes å copy, than adds it intp the new grid.
		for(int i = 0; i < this.columns; i++) {
			for(int j = 0; j < this.columns; j++) {
				CellState state = this.get(i, j);
				copyGrid.set(i, j, state);
				
			}
			
		}
    	
        return copyGrid;
    }
    
}
